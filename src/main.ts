import { NestFactory } from '@nestjs/core';
import { INestApplication } from '@nestjs/common';
import * as fs from 'fs';
import * as dotenv from 'dotenv';
import * as dotenvSafe from 'dotenv-safe';
import chalk from 'chalk';
import { ApplicationModule } from './modules/application.module';
import { appBootstrap } from './app';
import { LoggerService } from './modules/common/service/logger.service';

if (fs.existsSync('.env')) {
    dotenv.config();
    dotenvSafe.config({
        allowEmptyValues: true,
    });
}

async function bootstrap() {
    const logger: LoggerService = new LoggerService(ApplicationModule.name);

    const app: INestApplication = await NestFactory.create(ApplicationModule, {
        logger,
    });
    app.enableCors();

    await appBootstrap(app, logger);

    const port = (process.env.PORT as string) || 3000;
    await app.listen(port, () => {
        if (process.env.NODE_ENV !== 'production') {
            const divider = chalk.gray('-----------------------------------\t\t |');
            logger.log(divider);
            logger.log(`Service started ==> 🌎 \t\t\t\t |`);
            logger.log(`Localhost: \t${chalk.magenta(`http://localhost:${port}`)} \t\t |`);

            if (process.env.SWAGGER_ENABLED) {
                logger.log(`Swagger: \t${chalk.green(`http://localhost:${port}/api/doc`)} \t |`);
            }
            logger.log(divider);
        }
    });
}

bootstrap();
