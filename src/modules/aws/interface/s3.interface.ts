export interface IFile {
    originalname: string;
    buffer: Buffer;
}

export interface IS3Response {
    ETag: string;
    Location: string;
    Key: string;
    key: string;
    Bucket: string;
}
