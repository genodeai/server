import { Injectable } from '@nestjs/common';
import AWS from 'aws-sdk';
import uuid from 'uuid/v4';
import * as dotenv from 'dotenv';
import * as dotenvSafe from 'dotenv-safe';
import * as fs from 'fs';

import { IFile } from '.';
import { IS3Response } from './interface/s3.interface';

if (fs.existsSync('.env')) {
    dotenv.config();
    dotenvSafe.config({
        allowEmptyValues: true,
    });
}

AWS.config.update({
    region: process.env.AWS_REGION,
    apiVersion: process.env.AWS_API_VERSION,
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY_ID,
});

@Injectable()
export class S3Service {
    constructor() {
        this.s3 = new AWS.S3();
    }

    private readonly s3;

    async upload(file: IFile): Promise<IS3Response> {
        file.originalname = file.originalname.replace(/^.*\./i, `${uuid()}.`);
        return this.s3
            .upload({
                Key: `${process.env.AWS_S3_BUCKET_FOLDER}/${file.originalname}`,
                Body: file.buffer,
                Bucket: process.env.AWS_S3_BUCKET,
                ACL: 'public-read',
            })
            .promise();
    }
}
