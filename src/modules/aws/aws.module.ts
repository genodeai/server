import { Module, NestModule } from '@nestjs/common';
import { S3Service } from './s3.service';

@Module({
    providers: [S3Service],
    exports: [S3Service],
})
export class AWSModule implements NestModule {
    configure(): void {
        // consumer
        //     .apply(LoggerMiddleware)
        //     .with(CohortModule.name)
        //     .forRoutes({ path: '*', method: RequestMethod.ALL });
    }
}
