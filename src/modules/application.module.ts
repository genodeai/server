import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CorrelationMiddleware, LoggerMiddleware } from './common/middleware';
import { VehicleModule } from './vehicle/vehicle.module';
import { AWSModule } from './aws/aws.module';

@Module({
    imports: [TypeOrmModule.forRoot(), VehicleModule, AWSModule],
})
export class ApplicationModule implements NestModule {
    configure(consumer: MiddlewareConsumer) {
        consumer
            .apply(CorrelationMiddleware)
            // .with(ApplicationModule.name)
            .forRoutes('*');
        consumer
            .apply(LoggerMiddleware)
            .with(ApplicationModule.name)
            .forRoutes('*');

        return consumer;
    }
}
