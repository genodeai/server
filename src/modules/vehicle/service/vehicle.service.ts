import { Injectable } from '@nestjs/common';
import { Vehicle } from '../entity/vehicle.entity';
import { VehicleRepository } from '../repository/vehicle.repository';
import { S3Service } from '../../aws';
import { IUploadPictures } from '../interface';

@Injectable()
export class VehicleService {
    constructor(private readonly vehicleRepository: VehicleRepository, private readonly s3Service: S3Service) {}

    async create(vehicle: Vehicle) {
        const entity = this.vehicleRepository.create(vehicle);
        return this.vehicleRepository.save(entity);
    }

    async upload(vehicle: Vehicle, files: IUploadPictures = {}): Promise<Vehicle> {
        if (files.main && files.main[0]) {
            vehicle.pictures.main = await this.s3Service.upload(files.main[0]);
        }
        if (files.list) {
            const promises = files.list.map(item => this.s3Service.upload(item));
            vehicle.pictures.list = await Promise.all(promises);
        }
        return this.vehicleRepository.save(vehicle);
    }
}
