import { Controller, Post, Get, Param, HttpStatus, Body, UseInterceptors, HttpCode, UploadedFiles, FileFieldsInterceptor, Patch, Delete } from '@nestjs/common';
import { ApiOperation, ApiUseTags, ApiResponse, ApiImplicitBody, ApiImplicitParam } from '@nestjs/swagger';
import { TransformClassToPlain } from 'class-transformer';

import { Vehicle } from '../entity/vehicle.entity';
import { VehicleRepository } from '../repository/vehicle.repository';
import { VehicleService } from '../service/vehicle.service';
import { ValidateAndTransformPipe } from '../../common';
import { VehicleByUUIDPipe } from '../pipe/vehicle-by-uuid.pipe';
import { VEHICLE_PICTURES_LIST_LIMIT } from '../../common/constants';

const getValidateAndTransformPipe = (groups: string[] = [], customTransformClass?: any) => {
    const options: any = {
        validator: {
            groups,
            whitelist: true,
            forbidNonWhitelisted: true,
            forbidUnknownValues: true,
            validationError: { target: false, value: false },
        },
        transformer: {
            groups,
        },
    };
    if (customTransformClass) {
        options.customTransformClass = customTransformClass;
    }
    return new ValidateAndTransformPipe(options);
};

@Controller('api/v1/vehicles')
@ApiUseTags('vehicles')
export class VehicleController {
    constructor(private readonly vehicleRepository: VehicleRepository, private readonly vehicleService: VehicleService) {}

    @Post()
    @ApiOperation({ title: 'Create a Vehicle' })
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Validation error' })
    @ApiResponse({ status: HttpStatus.CONFLICT, description: 'Vehicle with this properties' })
    @ApiResponse({ status: HttpStatus.CREATED, type: Vehicle, description: 'New Vehicle has been successfully created' })
    @ApiImplicitBody({ name: 'Vehicle data', type: Vehicle, required: true })
    @TransformClassToPlain({ groups: ['read'] })
    async create(
        @Body(getValidateAndTransformPipe(['create'], Vehicle))
        vehicle: Vehicle,
    ): Promise<Vehicle> {
        return this.vehicleService.create(vehicle);
    }

    @Get()
    @ApiOperation({ title: 'Get Vehicle records with filter' })
    @ApiResponse({ status: HttpStatus.OK, type: Vehicle, description: 'OK' })
    @TransformClassToPlain({ groups: ['read'] })
    async findAll(): Promise<Vehicle[]> {
        return this.vehicleRepository.find();
    }

    @Get('/:uuid')
    @ApiOperation({ title: 'Get a Vehicle information by uuid' })
    @ApiResponse({ status: HttpStatus.OK, type: Vehicle, description: 'OK' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Record not found' })
    @ApiImplicitParam({ name: 'uuid', type: String })
    @TransformClassToPlain({ groups: ['read'] })
    async findOne(@Param('uuid') uuid: string): Promise<Vehicle> {
        return this.vehicleRepository.findOneOrFail({ uuid });
    }

    @Patch('/:uuid')
    @HttpCode(HttpStatus.OK)
    @ApiOperation({ title: 'Update Vehicle data' })
    @ApiImplicitParam({ name: 'uuid', type: String, required: true })
    @ApiImplicitBody({ name: 'Vehicle data', required: true, type: Vehicle })
    @ApiResponse({ status: HttpStatus.OK, type: Vehicle, description: 'The record has been successfully updated' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Record not found' })
    @ApiResponse({ status: HttpStatus.BAD_REQUEST, description: 'Validation error' })
    @TransformClassToPlain({ groups: ['read'] })
    async patch(
        @Param(VehicleByUUIDPipe) vehicle: Vehicle,
        @Body(getValidateAndTransformPipe(['update'], Vehicle))
        data: Partial<Vehicle>,
    ) {
        const mergedVehicle = this.vehicleRepository.merge(vehicle, data);

        return this.vehicleRepository.save(mergedVehicle);
    }

    @Delete('/:uuid')
    @HttpCode(HttpStatus.NO_CONTENT)
    @ApiOperation({ title: 'Delete Vehicle record' })
    @ApiImplicitParam({ name: 'uuid', type: String })
    @ApiResponse({ status: HttpStatus.NO_CONTENT, type: Vehicle, description: 'OK' })
    @ApiResponse({ status: HttpStatus.NOT_FOUND, description: 'Record not found' })
    async remove(@Param(VehicleByUUIDPipe) vehicle: Vehicle): Promise<void> {
        await this.vehicleRepository.remove(vehicle);
    }

    @Post('/:uuid/pictures')
    @HttpCode(HttpStatus.OK)
    @ApiOperation({ title: 'Upload picture by Vehicle uuid' })
    @ApiImplicitParam({ name: 'uuid', type: String })
    @ApiResponse({ status: HttpStatus.OK, type: Vehicle, description: 'OK' })
    @UseInterceptors(FileFieldsInterceptor([{ name: 'main', maxCount: 1 }, { name: 'list', maxCount: VEHICLE_PICTURES_LIST_LIMIT }]))
    @TransformClassToPlain({ groups: ['read'] })
    upload(@UploadedFiles() files, @Param(VehicleByUUIDPipe) vehicle: Vehicle): Promise<Vehicle> {
        return this.vehicleService.upload(vehicle, files);
    }
}
