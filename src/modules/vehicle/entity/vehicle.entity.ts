import { Entity, ObjectIdColumn, ObjectID, Column, CreateDateColumn, UpdateDateColumn, Index } from 'typeorm';
import { ApiModelProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { ConditionEnum, BodyStyleEnum, FuelEnum, TransmissionEnum, PowerTypeEnum, BrandEnum } from '../interface/vehicle.interface';
import {
    VehicleModel,
    LocationModel,
    BasicModel,
    ShapeModel,
    EngineModel,
    PowerModel,
    FeaturesModel,
    PicturesModel,
    S3ItemModel,
} from '../model/vehicle.model';

export class Location extends LocationModel {
    @Column()
    @ApiModelProperty({ type: String })
    name: string;

    @Column()
    @ApiModelProperty({ type: Number })
    lat: number;

    @Column()
    @ApiModelProperty({ type: Number })
    lng: number;
}

export class Basic extends BasicModel {
    @Column()
    @ApiModelProperty({ type: String })
    year: number;

    @Column()
    @ApiModelProperty({ type: Number })
    mileage: number;

    @Column()
    @ApiModelProperty({ type: Number })
    price: number;
}

export class Shape extends ShapeModel {
    @Column()
    @ApiModelProperty({ type: String, enum: Object.values(BodyStyleEnum) })
    body: BodyStyleEnum;

    @Column()
    @ApiModelProperty({ type: Number, minimum: 1 })
    doors: number;

    @Column()
    @ApiModelProperty({ type: Number, minimum: 1 })
    seats: number;
}

export class Power extends PowerModel {
    @Column()
    @ApiModelProperty({ type: Number, minimum: 0 })
    number: number;

    @Column()
    @ApiModelProperty({ type: String, enum: Object.values(PowerTypeEnum) })
    type: PowerTypeEnum;
}

export class Engine extends EngineModel {
    @Column()
    @ApiModelProperty({ type: String, enum: Object.values(FuelEnum) })
    fuel: FuelEnum;

    @Column()
    @ApiModelProperty({ type: String, enum: Object.values(TransmissionEnum) })
    transmission: TransmissionEnum;

    @Column()
    @ApiModelProperty({ type: Power })
    power: Power;

    @Column()
    @ApiModelProperty({ type: Number, minimum: 0 })
    capacity: number;
}

export class Features extends FeaturesModel {
    @Column()
    @ApiModelProperty({ type: String, isArray: true })
    interior: string[];

    @Column()
    @ApiModelProperty({ type: String, isArray: true })
    exterior: string[];
}

export class S3Item extends S3ItemModel {
    @Column()
    @Exclude()
    ETag: string;

    @Column()
    @ApiModelProperty({ type: String })
    Location: string;

    @Column()
    @Exclude()
    Key: string;

    @Column()
    @Exclude()
    Bucket: string;
}

export class Pictures extends PicturesModel {
    @Column()
    @ApiModelProperty({ type: String })
    main: S3Item;

    @Column()
    @ApiModelProperty({ type: String, isArray: true })
    list: S3Item[];
}

@Entity({ name: 'vehicles' })
export class Vehicle extends VehicleModel {
    @Exclude()
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    @Index('uuid', { unique: true })
    @ApiModelProperty({ type: String, readOnly: true, format: 'uuid', description: 'uuid v4', example: 'e12acc6f-2f34-442c-92d3-d4199fb12345' })
    uuid: string;

    @Column()
    @ApiModelProperty({ type: String, enum: Object.values(ConditionEnum) })
    condition: ConditionEnum;

    @Column()
    @ApiModelProperty({ type: String, enum: Object.values(BrandEnum) })
    brand: BrandEnum;

    @Column()
    @ApiModelProperty({ type: String })
    model: string;

    @Column()
    @ApiModelProperty({ type: String })
    color: string;

    @Column()
    @ApiModelProperty({ type: Location })
    location: Location;

    @Column()
    @ApiModelProperty({ type: Basic })
    basic: Basic;

    @Column()
    @ApiModelProperty({ type: Shape })
    shape: Shape;

    @Column()
    @ApiModelProperty({ type: Engine })
    engine: Engine;

    @Column()
    @ApiModelProperty({ type: Features })
    features: Features;

    @Column()
    @ApiModelProperty({ type: Pictures })
    pictures: Pictures;

    @Exclude()
    @CreateDateColumn()
    @ApiModelProperty({ type: String, format: 'date-time', readOnly: true })
    createdAt: Date;

    @Exclude()
    @UpdateDateColumn()
    @ApiModelProperty({ type: String, format: 'date-time', readOnly: true })
    updatedAt: Date;
}
