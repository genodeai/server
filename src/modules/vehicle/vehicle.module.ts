import { Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Vehicle } from './entity/vehicle.entity';
import { VehicleController } from './controller/vehicle.controller';
import { VehicleRepositoryProvider } from './repository/vehicle.repository';
import { VehicleService } from './service/vehicle.service';
import { VehicleSubscriber } from './subscriber';
import { AWSModule, S3Service } from '../aws';

@Module({
    imports: [TypeOrmModule.forFeature([Vehicle]), AWSModule],
    controllers: [VehicleController],
    providers: [VehicleRepositoryProvider, VehicleService, VehicleSubscriber, S3Service],
})
export class VehicleModule implements NestModule {
    configure(): void {
        // consumer
        //     .apply(LoggerMiddleware)
        //     .with(CohortModule.name)
        //     .forRoutes({ path: '*', method: RequestMethod.ALL });
    }
}
