import { IS3Response } from '../../aws/interface';

export interface IVehicle {
    condition: ConditionEnum;
    brand: BrandEnum;
    model: string;
    color: string;
    location: ILocation;
    basic: IBasic;
    shape: IShape;
    engine: IEngine;
    features: IFeatures;
    pictures: IPictures;
}

export interface ILocation {
    name: string;
    lat: number;
    lng: number;
}

export interface IBasic {
    year: number;
    mileage: number;
    price: number;
}

export interface IShape {
    body: BodyStyleEnum;
    doors: number;
    seats: number;
}

export interface IEngine {
    fuel: FuelEnum;
    transmission: TransmissionEnum;
    power: IPower;
    capacity: number;
}

export interface IPower {
    number: number;
    type: PowerTypeEnum;
}

export interface IFeatures {
    interior: string[];
    exterior: string[];
}

export enum ConditionEnum {
    NEW = 'new',
    USED = 'used',
}

export enum FuelEnum {
    PETROL = 'petrol',
    DIESEL = 'diesel',
    ELECTRIC = 'electric',
    ETHANOL = 'ethanol',
    HYBRID_DIESEL_ELECTRIC = 'hybrid (diesel/electric)',
    HYBRID_PETROL_ELECTRIC = 'hybrid (petrol/electric)',
    HYDROGEN = 'hydrogen',
    LPG = 'lpg',
    NATURAL_GAS = 'natural gas',
    OTHER = 'other',
}

export enum TransmissionEnum {
    AUTOMATIC = 'automatic',
    MANUAL = 'manual',
    SEMI_AUTOMATIC = 'semi-automatic',
}

export enum PowerTypeEnum {
    PS = 'ps',
    kW = 'kW',
}

export enum BodyStyleEnum {
    OFFROAD = 'offroad',
    CABRIOLET = 'cabriolet',
    ESTATE = 'estate',
    MINIBUS = 'minibus',
    SPORT = 'sport',
    SMALL = 'small',
    SALOON = 'saloon',
    OTHER = 'other',
}

export interface IPictures {
    main: IS3Response;
    list: IS3Response[];
}

export interface IUploadPictures {
    readonly main?: any[];
    readonly list?: any[];
}

export enum BrandEnum {
    SEAT = 'Seat',
    RENAULT = 'Renault',
    PEUGEOT = 'Peugeot',
    DACIA = 'Dacia',
    CITROEN = 'Citroën',
    OPEL = 'Opel',
    ALFA_ROMEO = 'Alfa Romeo',
    SKODA = 'Škoda',
    CHEVROLET = 'Chevrolet',
    PORSCHE = 'Porsche',
    HONDA = 'Honda',
    SUBARU = 'Subaru',
    MAZDA = 'Mazda',
    MISTUBISHI = 'Mitsubishi',
    LEXUS = 'Lexus',
    TOYOTA = 'Toyota',
    BMW = 'BMW',
    VOLKSWAGEN = 'Volkswagen',
    SUZUKI = 'Suzuki',
    MERCEDES_BENZ = 'Mercedes-Benz',
    SAAB = 'Saab',
    AUDI = 'Audi',
    KIA = 'Kia',
    LAND_ROVER = 'Land Rover',
    DODGE = 'Dodge',
    CHRYSLER = 'Chrysler',
    FORD = 'Ford',
    HUMMER = 'Hummer',
    HYUNDAI = 'Hyundai',
    INFINITI = 'Infiniti',
    JAGUAR = 'Jaguar',
    JEEP = 'Jeep',
    NISSAN = 'Nissan',
    VOLVO = 'Volvo',
    DAEWOO = 'Daewoo',
    FIAT = 'Fiat',
    MINI = 'MINI',
    ROVER = 'Rover',
    SMART = 'Smart',
}
