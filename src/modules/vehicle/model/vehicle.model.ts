import { IsString, IsNumber, ValidateNested, IsInt, IsEnum, Min, ArrayUnique, IsArray, IsNotEmpty, ArrayNotEmpty, Max } from 'class-validator';
import { Type, Expose, Exclude } from 'class-transformer';
import {
    IVehicle,
    ILocation,
    IBasic,
    IShape,
    BodyStyleEnum,
    IEngine,
    FuelEnum,
    TransmissionEnum,
    IPower,
    PowerTypeEnum,
    IFeatures,
    ConditionEnum,
    IPictures,
    BrandEnum,
} from '../interface/vehicle.interface';
import { IS3Response } from '../../aws/interface';

export class LocationModel implements ILocation {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsString({ groups: ['read', 'create', 'update'] })
    name: string;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsNumber({}, { groups: ['read', 'create', 'update'] })
    lat: number;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsNumber({}, { groups: ['read', 'create', 'update'] })
    lng: number;
}

export class BasicModel implements IBasic {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(1900, { groups: ['read', 'create', 'update'] })
    @Max(new Date().getFullYear(), { groups: ['read', 'create', 'update'] })
    year: number;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(0, { groups: ['read', 'create', 'update'] })
    mileage: number;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(1, { groups: ['read', 'create', 'update'] })
    price: number;
}

export class ShapeModel implements IShape {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsEnum(BodyStyleEnum, { groups: ['read', 'create', 'update'] })
    body: BodyStyleEnum;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(1, { groups: ['read', 'create', 'update'] })
    doors: number;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(1, { groups: ['read', 'create', 'update'] })
    seats: number;
}

export class PowerModel implements IPower {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(0, { groups: ['read', 'create', 'update'] })
    number: number;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsEnum(PowerTypeEnum, { groups: ['read', 'create', 'update'] })
    type: PowerTypeEnum;
}

export class EngineModel implements IEngine {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsEnum(FuelEnum, { groups: ['read', 'create', 'update'] })
    fuel: FuelEnum;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsEnum(TransmissionEnum, { groups: ['read', 'create', 'update'] })
    transmission: TransmissionEnum;

    @Expose({ groups: ['read', 'create', 'update'] })
    @ValidateNested({ groups: ['read', 'create', 'update'] })
    @Type(() => PowerModel)
    power: IPower;

    @Expose({ groups: ['read', 'create', 'update'] })
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsInt({ groups: ['read', 'create', 'update'] })
    @Min(0, { groups: ['read', 'create', 'update'] })
    capacity: number;
}

export class FeaturesModel implements IFeatures {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsArray({ groups: ['read', 'create', 'update'] })
    @ArrayUnique({ groups: ['read', 'create', 'update'] })
    interior: string[];

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsArray({ groups: ['read', 'create', 'update'] })
    @ArrayUnique({ groups: ['read', 'create', 'update'] })
    exterior: string[];
}

export class S3ItemModel implements IS3Response {
    @Exclude() ETag: string;

    @Expose({ groups: ['read'] })
    Location: string;

    @Exclude() Bucket: string;

    @Expose() Key: string;
    @Exclude() key: string;
}

export class PicturesModel implements IPictures {
    @Expose({ groups: ['read'] })
    @Type(() => S3ItemModel)
    main: S3ItemModel;

    @Expose({ groups: ['read'] })
    @IsArray({ groups: ['read'] })
    @ArrayNotEmpty({ groups: ['read'] })
    @Type(() => S3ItemModel)
    list: S3ItemModel[];
}

export class VehicleModel implements IVehicle {
    @Expose({ groups: ['read', 'create', 'update'] })
    @IsEnum(ConditionEnum, { groups: ['read', 'create', 'update'] })
    condition: ConditionEnum;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsEnum(BrandEnum, { groups: ['read', 'create', 'update'] })
    brand: BrandEnum;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsString({ groups: ['read', 'create', 'update'] })
    @IsNotEmpty({ groups: ['read', 'create', 'update'] })
    model: string;

    @Expose({ groups: ['read', 'create', 'update'] })
    @IsString({ groups: ['read', 'create', 'update'] })
    @IsNotEmpty({ groups: ['read', 'create', 'update'] })
    color: string;

    @Expose({ groups: ['read', 'create', 'update'] })
    @ValidateNested({ groups: ['read', 'create', 'update'] })
    @Type(() => LocationModel)
    location: LocationModel;

    @Expose({ groups: ['read', 'create', 'update'] })
    @ValidateNested({ groups: ['read', 'create', 'update'] })
    @Type(() => BasicModel)
    basic: BasicModel;

    @Expose({ groups: ['read', 'create', 'update'] })
    @ValidateNested({ groups: ['read', 'create', 'update'] })
    @Type(() => ShapeModel)
    shape: ShapeModel;

    @Expose({ groups: ['read', 'create', 'update'] })
    @ValidateNested({ groups: ['read', 'create', 'update'] })
    @Type(() => EngineModel)
    engine: EngineModel;

    @Expose({ groups: ['read', 'create', 'update'] })
    @ValidateNested({ groups: ['read', 'create', 'update'] })
    @Type(() => FeaturesModel)
    features: FeaturesModel;

    @Expose({ groups: ['read'] })
    @ValidateNested({ groups: ['read'] })
    @Type(() => PicturesModel)
    pictures: PicturesModel;
}
