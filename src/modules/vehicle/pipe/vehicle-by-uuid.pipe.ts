import { PipeTransform, Injectable } from '@nestjs/common';
import { VehicleRepository } from '../repository/vehicle.repository';
import { Vehicle } from '../entity/vehicle.entity';

@Injectable()
export class VehicleByUUIDPipe implements PipeTransform<Partial<Vehicle>, Promise<Vehicle>> {
    constructor(protected readonly vehicleRepository: VehicleRepository) {}

    async transform(params: Partial<Vehicle>) {
        return this.vehicleRepository.findOneOrFail({ uuid: params.uuid });
    }
}
