import { EventSubscriber, EntitySubscriberInterface, InsertEvent, UpdateEvent } from 'typeorm';
import uuid from 'uuid/v4';
import { Vehicle, S3Item } from '../entity';

@EventSubscriber()
export class VehicleSubscriber implements EntitySubscriberInterface<Vehicle> {
    listenTo() {
        return Vehicle;
    }

    async beforeInsert(event: InsertEvent<Vehicle>) {
        const now = new Date();

        event.entity.uuid = uuid();

        event.entity.createdAt = now;
        event.entity.updatedAt = now;

        event.entity.pictures = { main: new S3Item(), list: [] };
    }

    beforeUpdate(event: UpdateEvent<Vehicle>) {
        event.entity.updatedAt = new Date();
    }
}
