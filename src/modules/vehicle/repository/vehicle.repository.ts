import { Connection, EntityRepository, MongoRepository } from 'typeorm';
import { Vehicle } from '../entity/vehicle.entity';
import { RepositoryProvider } from '../../common/interface/repository.interface';

@EntityRepository(Vehicle)
export class VehicleRepository extends MongoRepository<Vehicle> {}

export const VehicleRepositoryProvider: RepositoryProvider = Object.freeze({
    provide: VehicleRepository.name,
    useFactory: (connection: Connection) => connection.getCustomRepository(VehicleRepository),
    inject: [Connection],
});
