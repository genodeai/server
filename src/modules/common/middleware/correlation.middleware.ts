import { Injectable, NestMiddleware } from '@nestjs/common';
import correlator from 'correlation-id';
import { ServerResponse, IncomingMessage } from 'http';
import { CORRELATION_HEADER } from '../constants';

export interface ICorrelationMiddlewareOptions {
    header: string;
}

@Injectable()
export class CorrelationMiddleware implements NestMiddleware {
    resolve(options?: ICorrelationMiddlewareOptions) {
        const params: ICorrelationMiddlewareOptions = {
            ...options,
            ...{
                header: CORRELATION_HEADER,
            },
        };

        return (req: IncomingMessage, res: ServerResponse, next: any) => {
            const correlationId = req.headers[params.header];
            if (correlationId) {
                correlator.withId(correlationId, () => {
                    res.setHeader(params.header, correlationId);
                    next();
                });
            } else {
                correlator.withId(next);
            }
        };
    }
}
