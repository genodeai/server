import { Injectable, NestMiddleware } from '@nestjs/common';
import { IncomingMessage, ServerResponse } from 'http';
// import correlator from 'correlation-id';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
    resolve(/* name: string */) {
        return (parameters: { req: IncomingMessage; res: ServerResponse; next: any }) => {
            const { next } = parameters;
            // console.log(`[${name}] LoggerMiddleware: request x-correlation-id=%s`, correlator.getId());
            next();
        };
    }
}
