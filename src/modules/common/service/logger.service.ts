import { Injectable, LoggerService as BaseLoggerService } from '@nestjs/common';
import correlator from 'correlation-id';
import { CORRELATION_HEADER } from '../constants';
import winston, { Logger, format } from 'winston';

const logger: Logger = winston.createLogger({
    level: process.env.LOG_LEVEL,
    format: format.combine(format.colorize(), format.timestamp(), format.align(), format.printf(info => `${info.timestamp} ${info.level}: ${info.message}`)),
    transports: [
        new (winston.transports.Console as any)({
            level: 'debug',
            colorize: true,
            handleExceptions: true,
            humanReadableUnhandledException: true,
            datePattern: '.yyyy-MM-dd',
            timestamp: true,
        }),
    ],
});

@Injectable()
export class LoggerService implements BaseLoggerService {
    private readonly context: string;
    private readonly correlationId: string;
    private readonly isTestEnv: boolean;

    constructor(context = 'ApplicationModule') {
        this.context = context;
        this.correlationId = correlator.getId();
        this.isTestEnv = 'test' === process.env.NODE_ENV;
    }

    log(message: string): void {
        if (this.isTestEnv) {
            return;
        }
        logger.debug(this.build(message));
    }
    info(message: string) {
        if (this.isTestEnv) {
            return;
        }

        logger.info(this.build(message));
    }
    error(message: string, trace?: string) {
        if (this.isTestEnv) {
            return;
        }

        logger.error(this.build(message, trace));
    }
    warn(message: string) {
        if (this.isTestEnv) {
            return;
        }

        logger.warn(this.build(message));
    }

    build(message, trace?) {
        const payload: any = { context: this.context, ...this.getMeta() };
        if (trace) {
            payload.trace = trace;
        }

        return `${message} ${JSON.stringify(payload)}`;
    }

    private getMeta(): object {
        const meta = {};

        if (this.correlationId) {
            meta[CORRELATION_HEADER] = this.correlationId;
        }

        return meta;
    }
}

export const LoggerServiceProvider = {
    provide: LoggerService.name,
    useFactory: (context = 'Application') => {
        return new LoggerService(context);
    },
};
