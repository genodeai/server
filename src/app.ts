import { INestApplication, LoggerService } from '@nestjs/common';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { Reflector } from '@nestjs/core';
import { DtoPipe } from 'nestjs-extensions';
import { GracefulShutdownManager } from '@moebius/http-graceful-shutdown';
import * as dotenv from 'dotenv';
import * as dotenvSafe from 'dotenv-safe';
import * as fs from 'fs';
import { TypeormEntityNotFoundErrorFilter } from './modules/common/filter/typeorm-entity-not-found-error.filter';

if (fs.existsSync('.env')) {
    dotenv.config();
    dotenvSafe.config({
        allowEmptyValues: true,
    });
}

export async function appBootstrap(app: INestApplication, logger: LoggerService): Promise<INestApplication> {
    bootstrapSwagger(app);

    app.useGlobalPipes(new DtoPipe(new Reflector()));
    app.useGlobalFilters(new TypeormEntityNotFoundErrorFilter());

    const shutdownManager = new GracefulShutdownManager(app.getHttpServer());

    process.on('SIGTERM', () => {
        shutdownManager.terminate(() => {
            logger.log('Server is gracefully terminated');
        });
    });

    return app;
}

function bootstrapSwagger(app: INestApplication) {
    if (process.env.SWAGGER_ENABLED) {
        const options = new DocumentBuilder()
            .setTitle('Portal')
            .setDescription('The portal API')
            .setVersion('1.0')
            .setSchemes((process.env.SWAGGER_SCHEME as 'http' | 'https') || 'http')
            .build();

        const document = SwaggerModule.createDocument(app, options);
        SwaggerModule.setup('/api/doc', app, document);
    }
}
